const { Router } = require('express');
const { check } = require('express-validator');
const { validar } =  require('../middlewares/validar_campos');
const { getAll, getOne, create, eliminar, update, activar, desactivar } = require('../controllers/proyectController');


const router = Router();

router.get('/project/list', getAll);
router.get('/project/:id', getOne);
router.post('/project/create', [
    check('nombre', 'El nombre es obligatorio').not().isEmpty(),
    check('descripcion', 'La descripción es obligatoria').not().isEmpty(),
    check('publicacion', 'La descripción es obligatoria').not().isEmpty(),
    check('cierre', 'El cierre es obligatorio').not().isEmpty(),
    check('criterios', 'El criterio es obligatorio').not().isEmpty(),
    check('recompensa', 'La recompensa es obligatoria').not().isEmpty().isFloat(),
    check('email', 'El email es obligatorio').not().isEmpty().isEmail(),
    check('telefono', 'El telefono es obligatorio').not().isEmpty().isMobilePhone(),
    check('id_autor', 'El id_autor es obligatorio').not().isEmpty().isNumeric(),
    validar
],
create);
router.delete('/project/:id', eliminar);
router.put('/project/:id', update);
router.put('/project/ac/:id', activar);
router.put('/project/in/:id', desactivar);

module.exports = router;
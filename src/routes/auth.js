const { Router } = require('express');
const { renuevaToken, registro, login } = require('../controllers/authController');
const { valitarJWT } = require("../middlewares/validar-jwt");
const { check } = require('express-validator');
const { validar } =  require('../middlewares/validar_campos');

const router = Router();
   
router.post('/insertar', [
    check('temperatura','La temperatura es obligatoria').not().isEmpty(),
    check('ritmo','El ritmo es obligatorio').not().isEmpty(),
    check('presion','La presión es obligatoria').not().isEmpty(),
    check('estado','El estado es obligatorio').not().isEmpty(),
    check('nombre','El nombre es obligatorio').not().isEmpty(),
    check('carrera','La carrera es obligatoria').not().isEmpty(),
    check('matricula','La matricula es obligatoria').not().isEmpty(),
    check('horaR','La hora de registro es obligatoria').not().isEmpty(),
    validar
],
registro);

module.exports = router;
